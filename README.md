# Tanel Trull

## About me
Hi! My name is Tanel Trull and I am a junior QA Specialist. I'm deligthed to share with you my experience and projects that I have had the pleasure to work on so far.
<center>

![profile](Profile_Pic.jpg)

</center>

## Software testing
Software testing is an invariably important part of the software development process and paramount to achieve good quality product. My first steps towards software testing were made quite unconsciously, using various applications in which I had the opportunity to find some inaccuracies. I have previous work experience in medical coding and clinical data management field which perfectly supports the QA mindset. I am a person who likes to find all possible problems, and at the same time I focus on details. 

## Software Development Academy course
I had the pleasure to participate in the "Software Tester" course organized by Software Development Academy. During 105 hours of classes and many hours devoted to independent work, I gained knowledge on the following topics: #Test design techniques #Risk-based testing #Testing tools #Agile and Waterfall #Introduction to programming #Testing in the BDD methodology #Selenium WebDriver Fundamentals
<center>

[SDA_Certificate_-_Software-tester](SDA_Certificate_-_Software-tester.pdf)

</center>

Participating in the course also enabled me to navigate efficiently in agile projects thanks to the classes introducing the Scrum methodology:

<center>

[Scrum Workshops Diploma](scrum-workshops.pdf)

</center>

## Git and HTTP
During the course, I not only learned testing, but also developed my skills in many other directions, including:

* I learned to work with the GIT Tool (and Gitlab)

* I learned the basics of REST API and networking tools, thanks to which I will be able to provide support during the back-end debugging process in my future work.

## ISTQB certificate
During the course, we were preparing for the ISTQB Foundation Level exam. I went on to take the exam in the Estonian Testers Association's office in Tallinn and with a 95% result acquired the ISTQB CTFL certification.

[ISTQB CTFL certificate](ETL00870-Tanel_Trull.pdf)

## Tasks that I performed during the course:
<center>

Programming Fundamentals | Databases Fundamentals | Test Design Techniques | Testing Classes | Selenium WebDriver | Final Project

</center>

## My Projects

* Manual testing project for an e-commerece website [Final Project](Final_Project_-_Tanel_Trull.pdf)


## Technical Skills

<center>

TestRail | Selenium IDE | Jira | Photoshop | PyCharm | IntelliJ IDEA | Postman

</center>

## Interests
Testing is not everything, in my spare time I go to orienteering events, play with my miniature scnhauzer Frodo (yes, I'm a considerable LOTR geek), play computer games and nerd over modern European history. 

<center>


</center>

## Contact

Contact me by e-mail: trulltanel@gmail.com

Linkedin: [Tanel Trull](https://www.linkedin.com/in/tanel-trull-8b56a4173/)