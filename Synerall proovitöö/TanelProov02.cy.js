describe('Searching Materials', () => {

    it('Search for Proovitoo', () => {
      
        cy.visit("https://e-koolikott.ee/et/search") //Mine koolikoti otsinuglehele

        cy.get('#search').should('be.visible') //Veendu, et otsingu väli on nähtaval
        cy.get('#search').type('Proovitöö') //Trüki otsingusse "Proovitöö"
        cy.get('[type="submit"] > .mdi').click() //Käivita otsing (luubi ikoonil klikkides)

        
        cy.get('.search-material-filter-action--right > .mdi').click().click().click().click().click() //Kliki otsinguvälja aluse karussell-menüü parempoolsel noolel 5 korda
        cy.get('#musicFilterBtn > .search-material-filter-btn > .search-material-filter-btn-icon-wrap > .search-material-filter-btn-icon > .mdi').should('be.visible') //Veendu, et Heli filter on nähtaval
        cy.get('#musicFilterBtn > .search-material-filter-btn > .search-material-filter-btn-icon-wrap > .search-material-filter-btn-icon > .mdi').click() //Kliki Heli filtril
        cy.get('.search-active-filters > :nth-child(1)').should('be.visible') //Veendu, et Heli filter on aktiveeritud


        cy.get(':nth-child(2) > .tree-node > .tree-node-content > .node-content-wrapper > .taxon-title-label').should('be.visible') //Veendu, et Põhihariduse filter on nähtaval
        cy.get(':nth-child(2) > .tree-node > .tree-node-content > .mr-1', {waitForAnimations: false}).click() //Kliki Põhihariduse filtri puu avamiseks
        cy.get('.tree-children > tree-node-collection > [style="margin-top: 0px;"] > :nth-child(4) > .tree-node > .tree-node-content > .node-content-wrapper > .taxon-title-label').should('be.visible') //Veendu, et Kunstiainete filter on nähtaval
        cy.get('.tree-children > tree-node-collection > [style="margin-top: 0px;"] > :nth-child(4) > .tree-node > .tree-node-content > .mr-1', {waitForAnimations: false}).click() //Kliki Kunstiainete filtri alampuu avamiseks
        cy.get(':nth-child(4) > :nth-child(1) > :nth-child(2) > .tree-children > tree-node-collection > [style="margin-top: 0px;"] > :nth-child(1) > .tree-node > .tree-node-content > .node-content-wrapper > .taxon-title-label').should('be.visible') //Veendu, et Muusika filter on nähtaval
        cy.get(':nth-child(4) > :nth-child(1) > :nth-child(2) > .tree-children > tree-node-collection > [style="margin-top: 0px;"] > :nth-child(1) > .tree-node > .tree-node-content > .mr-1').click() //Kliki Muusika filtri aktiveerimiseks
        cy.get('.search-active-filters > :nth-child(3)').should('be.visible') //Veendu, et Muusika filter on aktiveeritud


        cy.get('kott-add-grade > .row > .col').scrollIntoView() //Scrolli seni kuni Klassi liugur tuleb nähtavale 
        cy.get('.col > .ngx-slider > .ngx-slider-pointer-min').should('be.visible') //Veendu, et Klassi liuguri miinimumväärtust määrav nupuke on nähtaval
        cy.get('.col > .ngx-slider > .ngx-slider-pointer-min').type("{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}{rightarrow}") //Vajuta klaviatuuril parema noole nuppu 6 korda
        cy.get('.col > .ngx-slider > .ngx-slider-pointer-min').should('have.attr', 'aria-valuenow', 7) //Veendu, et liuguri miinimumväärtuse nupu hetkeväärtus on 7


        cy.get('#keyCompetenceCollapse > :nth-child(2) > .ng-untouched > .checkbox-container > .checkbox').click() //Kliki Kultuuri- ja väärtuspädevuse filtri kastikesse
        cy.get('.search-active-filters > :nth-child(4)').should('be.visible') //Veendu, et Kultuuri- ja väärtuspädevuse filter on aktiveeritud


        cy.get('.material-card-desc').should('be.visible') //Veendu, et otsingutulemusena on nähtaval vähemalt üks materjali kaart
        cy.get('.material-card-desc').should("have.attr", "href", "/et/oppematerjal/31832-Proovitoo-materjal") //Veendu, et otsingutulemuseks saadud materjali kaart sisaldab ka nõutud hüperliki

    })

  })